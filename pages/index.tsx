import dynamic from "next/dynamic";

const Home = dynamic(() => import('../src/components/Torrent/TorrentClient').then((mod) => mod.TorrentClient), {
	ssr: false
})

export default Home;