import { useEffect } from 'react';

import WebTorrent from 'webtorrent/webtorrent.min.js'

const downloadFile = (client: WebTorrent) => {
	var magnetURI = 'magnet:?xt=urn:btih:e80a3cc3c7831a4b1e17c9d9b2e0e9aada93e797&dn=[Ohys-Raws]+Magatsu+Wahrheit+Zuerst+-+10+(BS11+1280x720+x264+AAC).mp4&tr=http://nyaa.tracker.wf:7777/announce&tr=udp://open.stealth.si:80/announce&tr=udp://tracker.opentrackr.org:1337/announce&tr=udp://tracker.coppersurfer.tk:6969/announce&tr=udp://exodus.desync.com:6969/announce&tr=wss://tracker.openwebtorrent.com&tr=wss://tracker.fastcast.nz&tr=wss://tracker.sloppyta.co'

	client.add(magnetURI, function (torrent) {
		// Got torrent metadata!
		console.log('Client is downloading:', torrent.infoHash)
	
		torrent.files.forEach(function (file) {
			// Display the file by appending it to the DOM. Supports video, audio, images, and
			// more. Specify a container element (CSS selector or reference to DOM node).
			file.appendTo('#video-container')
		})
	})
}

const VideoContainer = () => {
	let client = null;
	
	useEffect(() => {
		client = new WebTorrent()
	}, []);
	
	return (
		<>
			<p>Video</p>
			<section id="video-container" />
	
			<button onClick={() => downloadFile(client)}>
				Download
			</button>
			
	</> 
	)
}

export { VideoContainer }