import { createContext } from "react";

import WebTorrentClient, { Instance } from "webtorrent";
import { Torrent } from "./types";

export type TorrentContextProps = {
	client: Instance;
	torrents: Map<string, Torrent>,
}

const TorrentContext = createContext<TorrentContextProps>({
	client: null,
	torrents: null
})

export { TorrentContext }