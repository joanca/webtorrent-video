import React, { useState, FunctionComponent, Dispatch, SetStateAction, createContext, useEffect, useContext } from 'react';

import WebTorrentClient, { Instance } from 'webtorrent'

import { TorrentContext, TorrentContextProps } from './Context';

import { TorrentContainer } from './Torrent';
import { Torrent, TorrentFile } from './types';

const TorrentClient: FunctionComponent = () => {
	const client = new WebTorrentClient();
	const [torrents, setTorrents] = useState<Map<string, Torrent>>(new Map())

	useEffect(() => {
		client.on('torrent', (torrent) => {
			console.log("torrent added", torrent.infoHash)

			console.log(torrent.files[0])

			torrent.on('download', () => {
				const parseFile = (file: WebTorrentClient.TorrentFile): TorrentFile => ({
					name: file.name,
					length: file.length,
					progress: file.progress,
					appendTo: (elem) => file.appendTo(elem)
				})

				const newTorrent: Torrent = {
					name: torrent.name.toString(),
					infoHash: torrent.infoHash.toString(),
					progress: torrent.progress,
					done: torrent.progress === 1,
					downloadSpeed: torrent.downloadSpeed,
					uploadSpeed: torrent.uploadSpeed,
					files: torrent.files.map(parseFile)
				}
				
				setTorrents(torrents => new Map([...torrents, [newTorrent.infoHash, newTorrent]]));
			})
		})
	})

	return (
		<TorrentContext.Provider value={{client, torrents}}>
			<Container />
		</TorrentContext.Provider> 
	)
}

const Container: FunctionComponent = () => {
	const [torrentId, setTorrentId] = useState('');
	const { client, torrents } = useContext(TorrentContext);

	const addTorrent = (torrentId: string) => client.add(torrentId)

	const torrentList = Array.from(torrents.values())

	return (
		<>
			<p>File</p>
			<section id="file-container" />

			<input onChange={({ target }) => setTorrentId(withTrackers(target.value))} placeholder="magnet or hash"/>
			<br />
			<button onClick={() => addTorrent(torrentId)}>Add Torrent</button>
			<br />
			<br />

			<span>magnet link: {torrentId}</span>
			<br />
			<br />
			{torrentList.map(torrent => <TorrentContainer 
				key={torrent.infoHash} 
				{...torrent} 
			/>)}
		</>
	)
}

const withTrackers = (magnet: string) => {
	const decodedMagnet = decodeURIComponent(magnet);
	const trackers = "&tr=wss://tracker.btorrent.xyz&tr=wss://tracker.openwebtorrent.com&tr=wss://tracker.fastcast.nz&tr=wss://tracker.sloppyta.co";

	return decodedMagnet + trackers;
}

export { Container, TorrentClient }