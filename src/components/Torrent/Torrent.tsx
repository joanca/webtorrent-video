import { FunctionComponent, useEffect } from 'react';

import { Torrent, TorrentFile } from './types';

const TorrentContainer: FunctionComponent<Torrent> = (torrent) => {
	return torrent.infoHash && (
		<>
		{`download speed: ${torrent.downloadSpeed}`}
		<br />
		{`progress: ${torrent.progress}`}
		{torrent.files.map(file  => <File key={`${torrent.infoHash}-${file.name}`} {...file} />)}
		</>
	)
}

const File: FunctionComponent<TorrentFile> = (props) => {
	const { name, progress, appendTo } = props;

	useEffect(() => appendTo('#video-container'), []);

	return (
		<>
			<h3 key={name}>File: {name}</h3>
			<p>Downloaded: {progress}</p>

			<section id="video-container" />
		</>
	)
}

export { TorrentContainer }