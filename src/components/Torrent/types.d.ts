export type Torrent = {
	name: string,
	infoHash: string,
	done: boolean,
	downloadSpeed: number,
	uploadSpeed: number,
	progress: number,
	files: TorrentFile[]
}

export type TorrentFile = {
	name: string,
	length: number,
	progress: number,
	appendTo: (rootElem) => void
}